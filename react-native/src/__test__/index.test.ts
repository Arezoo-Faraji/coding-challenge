import { createCategories, truncateName, truncateDate, isNewProduct } from "../utils";


describe("test app functions", () => {

  test("returns an array of words that are separated with comma", () => {
    expect(createCategories("soja milk, price")).toEqual(["soja","milk"]);
  });
  test("returns truncated name with maximum 14 character", () => {
    expect(truncateName("Soja milk product from bio markt")).toEqual("Soja milk p...");
  });
  test("returns the whole name of product if it has less than 16 character", () => {
    expect(truncateName("Banana")).toEqual("Banana");
  });
  test("returns a Date instead od date time value", () => {
    expect(truncateDate("2021-08-23T08:41:54.000Z")).toEqual("23.08.2021");
  });
  test("returns true if the difference between today and posted dates is less than 7 days", () => {
    jest
      .useFakeTimers()
      .setSystemTime(new Date("2022-09-13"));
    expect(isNewProduct("2022-09-19T08:41:54.000Z")).toBe(true);
  });
  test("returns false if the difference between today and posted dates is more than 7 days", () => {
    jest
      .useFakeTimers()
      .setSystemTime(new Date("2022-09-20"));
    expect(isNewProduct("2021-09-19T08:41:54.000Z")).toBe(false);
  });


});