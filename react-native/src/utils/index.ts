import moment from "moment";

export const createCategories = (categories: string): string[] => {
  if (categories !== undefined && categories.length > 0) {
    const tags = categories.split(/[, ]+/);
    return tags.slice(0, 2);
  } else {
    return [];
  }
};

export const isNewProduct = (date: string): boolean => {
  if (date !== undefined) {
    let given = moment(date);
    let current = moment().startOf("day");
    let result = moment.duration(given.diff(current)).asDays();
    if (result <= 7 && result >= 0) {
      return true;
    } else {
      return false;
    }
  }
  return false;
};
export const truncateDate = (d: string): string => {
  if (d !== undefined) {
    let date = new Date(d);
    let formattedDate = moment(date).format("DD.MM.YYYY");
    return formattedDate;
  } else {
    return "";
  }
};
export const truncateName = (name: string): string => {
  if (name !== undefined && name.length > 0) {
    if (name.length < 16) {
      return name;
    } else {
      const result = name.slice(0, 11) + "...";
      return result;
    }
  } else {
    return "no Name";
  }
};

