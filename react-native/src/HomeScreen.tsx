import {MaterialCommunityIcons} from "@expo/vector-icons";
import React, {useEffect, useCallback} from "react";
import {RefreshControl, ScrollView, StyleSheet, View, FlatList} from "react-native";
import {Appbar, FAB} from "react-native-paper";
import {useSelector, useDispatch} from "react-redux";
import {selectors, actions} from "./store/inventory";
import {RootState} from "./store";
import {SafeAreaView} from "react-native-safe-area-context";
import {StackScreenProps} from "@react-navigation/stack";
import Product from "./Product";
import {truncateName, truncateDate, createCategories} from "./utils/index";
import {
    useFonts,
    Roboto_900Black, Roboto_400Regular
} from "@expo-google-fonts/roboto";

export default (props: StackScreenProps<{}>) => {
    const fetching = useSelector((state: RootState) => state.inventory.fetching);
    const inventory = useSelector(selectors.selectInventory);
    const dispatch = useDispatch();

    let [fontsLoaded] = useFonts({
        Roboto_900Black, Roboto_400Regular
    });


    useEffect(() => {
        const unsubscribe = props.navigation.addListener("focus", () => {
            dispatch(actions.fetchInventory());
        });

        return unsubscribe;
    }, [props.navigation]);

    const renderItem = useCallback((record) => (
        <Product productImage={record.fields["Product Image"]}
                 productName={truncateName(record.fields["Product Name"])}
                 productCategories={createCategories(record.fields["Product Categories"])}
                 posted={truncateDate(record.fields["Posted"])}/>
    ), []);
    if (!fontsLoaded) {
        return null;
    }


    return (
        <View style={{flex: 1}}>
            <Appbar.Header style={styles.header}>
                <Appbar.Content title="Inventory"/>
            </Appbar.Header>
            <ScrollView
                style={{flex: 1, backgroundColor: "#FFFFFF",}}
                refreshControl={
                    <RefreshControl
                        refreshing={fetching}
                        onRefresh={() => dispatch(actions.fetchInventory())}
                    />
                }

            >
                {fontsLoaded ?
                    <View style={styles.container}>
                        <FlatList
                            data={inventory}
                            keyExtractor={({id}) => id}
                            renderItem={({item}) => renderItem(item)}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    : <View></View>}

            </ScrollView>

            <SafeAreaView style={styles.fab}>
                <FAB
                    icon={() => (
                        <MaterialCommunityIcons
                            name="barcode"
                            size={24}
                            color="#0B5549"
                        />
                    )}
                    label="Scan Product"
                    onPress={() => props.navigation.navigate("Camera")}
                />
            </SafeAreaView>
        </View>);

}

const styles = StyleSheet.create({
    header: {
        width: "100%",
        height: 100,
        left: 0,
        top: 0
    },
    fab: {
        position: "absolute",
        bottom: 16,
        width: "100%",
        flex: 1,
        alignItems: "center"
    },
    container: {
        position: "absolute",
        width: "100%",
        height: 688,
        padding: 0,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        top: 16,
        bottom: 8,
        backgroundColor: "#FFFFFF",
    }
});
