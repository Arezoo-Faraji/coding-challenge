import React from "react";
import { Image, StyleSheet, View, Text } from "react-native";
import { isNewProduct } from "./utils/index";

interface ProductProps {
  posted: string,
  productCategories: string[],
  productImage: string,
  productName: string,
}

export default ({ posted, productCategories, productImage, productName }: ProductProps) => {

  return (
    <View style={[styles.frame, styles.shadowProps]}>
      {
        productImage === undefined ? (<View style={styles.placeholderFrame}>
              <Image
                source={require("../assets/placeholder.png")}
                style={styles.placeholder}
              />

            </View>

          )
          : <Image
            source={{ uri: productImage }}
            style={styles.image}
          />
      }
      <View style={{ marginLeft: 113 }}>
        <View style={{ display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "space-between" }}>
          <View style={styles.textContainer}>
            <Text style={styles.title}>{productName}</Text>
            <Text style={styles.date}>{posted}</Text>
          </View>
          {isNewProduct(posted) ? (<View style={styles.newTagFrame}><Text
            style={styles.newText}>New</Text></View>) : (
            <View></View>)}
        </View>
        <View style={styles.tagsContainer}>
          {
            productCategories.map((item, index) => {
              return (<View style={styles.tagFrame} key={index}><Text>{item}</Text></View>
              );
            })
          }
        </View></View>

    </View>
  );

}

const styles = StyleSheet.create({
  frame: {
    width: 343,
    maxWidth: "100%",
    height: 128,
    backgroundColor: "#F8F9FC",
    borderRadius: 4,
    alignItems: "flex-start",
    flexGrow: 0,
    padding: 8,
    margin: 8,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  shadowProps: {
    shadowColor: "#1B2633",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.60,
    elevation: 3
  },
  image: {
    position: "absolute",
    width: 85,
    height: 112,
    marginVertical: 8,
    left: 8,
    right: 250
  },
  placeholder: {
    width: 48.88,
    height: 48.88,
    position: "absolute",
    marginHorizontal: 15.56,
    top: 15,
    bottom: 16.2

  },
  placeholderFrame: {
    marginVertical: 24

  },
  textContainer: {
    color: "#1B2633",
    height: 42,
    display: "flex",
    alignItems: "flex-start",
    padding: 0,
    maxWidth: "70%",
    width: 140,
    top: 8,
    marginHorizontal: 8
  },
  title: {
    fontFamily: "Roboto_900Black",
    fontSize: 20,
    lineHeight: 22
  },
  date: {
    fontFamily: "Roboto_400Regular",
    fontSize: 12,
    lineHeight: 16,
    width: 160,
    height: 18,
    top: 2
  },
  tagsContainer: {
    width: 226,
    maxWidth: "100%",
    height: 58,
    top: 12,
    bottom: 8,
    left:8,
    right:19,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    padding: 0,
    flexWrap: "wrap"
  },
  tagFrame: {
    marginRight: 4,
    marginBottom: 6,
    backgroundColor: "#D4E5FF",
    paddingVertical: 2,
    paddingHorizontal: 12,
    borderRadius: 48,
    flexGrow: 0,
    alignItems: "center",
    justifyContent: "center",
    height: 26
  },
  tagName: {
    fontFamily: "Roboto_400Regular",
    fontSize: 12,
    lineHeight: 22,
    justifyContent: "center",
    textAlign: "center",
    marginVertical: 2,
    marginHorizontal: 12
  },
  newTagFrame: {
    width: 53,
    height: 34,
    top: 8,
    marginHorizontal: 2,
    bottom: 20,
    alignItems: "center",
    justifyContent: "center",
    padding: 12,
    borderBottomLeftRadius: 9,
    borderBottomRightRadius: 9,
    borderTopLeftRadius: 9,
    borderTopRightRadius: 0,
    backgroundColor: "#333333"
  },
  newText: {
    color: "#FFFFFF",
    width: 29,
    height: 15,
    fontFamily: "Roboto_400Regular"
  }
});